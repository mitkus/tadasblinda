You can move through crossbow guards, as they have no melee weapon, but if they see you they will try to sidestep and shoot you (make sure they don't with smoke or invisibility).

You can also move under dragons the same way, they might even have an item or two in its hoard (same square as the dragon).

A slain dragon, however, is sure to drop another item. They are fairly tough though.

Blink is always 5 squares and goes through anything.

Invisibility lasts 8 turns, but guards will still kill you if they catch you. And crossbowmen might shoot at the location you were seen in last.

Crossbow bolts cannot tell friend from foe. You can even shoot yourself with blink or haste.

Doppelganger potions are rare, but have no duration limit.

Guards patrol between only a few points, but there are no guarantees they will always take the same path.

Crossbowmen can shoot after moving, but only if the square they entered was empty.

Haste always gives you 5 rounds, but you never know when exactly. Except the first round of haste always happens in the turn you drank the potion.

You can drink multiple invisibility or haste potions in a row and their duration will add up. Handy when you're going for the dragon. 

If a guard is entering your square, you can use a smoke bottle to evade his attack.

Hiding in a pot protects against everything except boulders.

Smoke is unreliable - it might disappear in a turn, but could also last for a long while.

If you and your doppelganger enter the exit in the same turn, both will appear in the next level (keeping both inventories).
