#!/usr/bin/env python3

import sys, os, pickle
import math
import pygame
from lib import textmode
from lib import level
from lib import utils, menu, level_editor
from lib import inventory
from lib import effects, effects2
from lib import entity
from pygame.locals import *

width, height = 80, 25

def setup_game(gamemode, tier, level_file=None):
    lvl = level.Level(width, height, gamemode, tier, level_file)
    if gamemode != utils.gamemodes[5]:
        if lvl.players == []: return None
        # do one tick of "wait" action
        lvl.tick([utils.no_action])
    return lvl

def main():
    pygame.init()
    #+2 to add a row on top and on bottom for messages and status
    console = textmode.Textmode(width, height+2, 18)
    w, h = console.width * console.font_width, console.height * console.font_height

    screen = pygame.display.set_mode((w, h))
    pygame.display.set_caption('tadasblinda')

    console.selected_fg = pygame.Color(196, 196, 196)
    effects2.animator=effects2.Animator(console, screen, w, h)
    clockmaster=pygame.time.Clock()
    pygame.key.set_repeat(200,100)

    # Main loop.
    done = False
    game_start = True
    lvl_editor = None
    lvledit = False
    cont = False
    gamedata=None
    #attempt to load a session file
    try:
        with open('session.dat','rb') as session_save:
            gamemode, game_mode, gamedata, lvl, tier, show_inventory, endscreen, victory = pickle.load(session_save)
        os.remove('session.dat')
        cont = True
    except FileNotFoundError: gamemode = None
    except ValueError:
        print("Session data corrupt")
        gamemode = None
        
    while not done:
        #if game not started, show menu
        if game_start:
            menu_data=menu.show_menu(console, screen, gamemode, cont)
            game_start = False
            need_redraw = True
            help = False
            if menu_data is None:
                done = True
                continue
            elif menu_data[0]==5:
                #level editor
                if lvl_editor is None: lvl_editor = level_editor.Level_Editor(width, height, console, screen)
                gamemode=menu_data[0]
            elif menu_data[0]>=0:
                #game starting!
                endscreen = False
                victory = False
                cont = True
                show_inventory = False
                tier = 0
                game_mode=utils.gamemodes[menu_data[0]]
                lvl = setup_game(game_mode, tier, menu_data[1])
                if not lvl:
                    game_start = True
                    gamemode = 4
                    continue
                gamedata=menu_data[1]
                gamemode=menu_data[0]
            else:
                gamemode=utils.gamemodes.index(game_mode)
                lvl.request_player_markers()
                
        #start level editor if it is the selected game mode
        if gamemode == 5:
            if lvl_editor.main() is None: done = True
            game_start = True
            continue
            
        #main game loop
        actions = []

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            elif event.type == KEYDOWN:
                mods = pygame.key.get_mods()
                if (event.key == K_F4 and (mods & KMOD_ALT)) or (event.key == K_q and (mods & KMOD_CTRL)):
                    done = True
                if event.key == K_ESCAPE:
                    game_start = True
                    continue
                elif event.key == K_F1:
                    help = not help
                    need_redraw = True
                    # draw a player marker when turning off help
                    if not help and lvl and lvl.player_act_queue[lvl.current_player]:
                        lvl.player_act_queue[lvl.current_player].marker = True
                elif not (endscreen or event.key == K_BACKSPACE):
                    if event.key == K_TAB:
                        show_inventory = not show_inventory
                        need_redraw = True
                    else:
                        for a in utils.actions:
                            for key in a[1]:
                                if event.key == key:
                                    actions.append(a)
                elif event.key in (K_BACKSPACE, K_KP_ENTER, K_RETURN, K_SPACE):
                    # Endscreen - reset game
                    if victory:
                        old_inv=[player.inventory for player in filter(lambda player: player.victory, lvl.players)]#not deleting inventory if victory
                        tier+=1
                        if gamemode!=utils.gamemodes.index("Custom levels"): menu.save_score(game_mode, tier) #saving highscore as it's reached
                        else: menu.save_score(gamedata['selected_levels'], tier)
                        lvl = setup_game(game_mode, tier, gamedata)
                        if lvl is None:
                            if gamemode==utils.gamemodes.index("Custom levels"): menu.save_score(gamedata['selected_levels'], float(tier))
                            gamemode=None
                            game_start=True
                            break
                        lvl.players_wanted(len(old_inv))
                        for x in range(len(old_inv)): lvl.players[x].inventory=old_inv[x]
                        endscreen = False
                        victory = False
                        console.clear()
                        need_redraw = True
                    else:
                        game_start = True
                        gamemode = utils.gamemodes.index(game_mode)
                        need_redraw = False

    
        if lvl is not None and lvl.tick(actions):
            need_redraw = True
            if lvl.end or lvl.victory:
                victory = lvl.victory
                endscreen = True
    
        if effects2.animator.need_redraw:
            need_redraw=True
            effects2.animator.need_redraw=False
   
        if need_redraw:
            need_redraw = False
            console.clear()
            lvl.render(console)
            lvl.tint.render(console)
            if lvl.players: lvl.player_act_queue[lvl.current_player].inventory.render(console, show_inventory)

            if endscreen and not victory:
                console.fg_color = pygame.Color(200, 200, 200)
                console.print(35, 11, "--------------")
                console.print(35, 12, "| Game over. |")
                console.print(35, 13, "--------------")
                cont = False

            elif endscreen and victory:
                console.fg_color = pygame.Color(200, 200, 200)
                console.print(35, 11, "--------------")
                console.print(35, 12, "|  Victory!  |")
                console.print(35, 13, "--------------")
            

            screen.fill(pygame.Color(0,0,0))
            if help: menu.show_help(console, screen, True)
            console.render(screen)
            if lvl.player_act_queue[lvl.current_player].marker: 
                lvl.player_act_queue[lvl.current_player].marker = False
                effects2.animator.selection_circle(lvl.player_act_queue[lvl.current_player].x, lvl.player_act_queue[lvl.current_player].y)
        pygame.display.flip()
        clockmaster.tick(60) #enforces maximum framerate (and tickrate), so my cpu fans don't turn on when nothing is happening
    if cont:
        with open('session.dat','wb') as session_save:
            pickle.dump((gamemode, game_mode, gamedata, lvl, tier, show_inventory, endscreen, victory),session_save)
    pygame.display.quit()

if __name__ == "__main__":
    main()
                
