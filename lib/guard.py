import pygame
from lib import entity, utils, player, traps, inventory
import random, math
from enum import Enum

class Guard_types(Enum):
    patroling=0
    lazy=1
    crossbow=2

class Hostile(entity.Entity):
    """An abstract class to hold vision methods."""
    def __init__(self,x,y):
        super().__init__(x, y)
        self.viewcone={}
        self.look_direction=random.choice(utils.offsets4)
        self.nearest_player=None
    
    def _regen_viewcone(self,lvl,peripheral_vision=True,range=6,view_angle=math.pi/2):
        """Calculate the viewcone."""
        def is_visible(x,y):
            return not lvl.is_opaque(x,y)
        def add_dir_tile(direction):
            x,y=direction
            x+=self.x
            y+=self.y
            if is_visible(x,y):
                self.viewcone[(x,y)]=True
        self.viewcone=utils.viewcone(self.x,self.y,is_visible,range,self.look_direction,view_angle)
        #add periferal vision
        if peripheral_vision and is_visible(self.x, self.y):
            dir_index=utils.offsets4.index(self.look_direction)
            dir_index_next=(dir_index+1)%len(utils.offsets4)
            add_dir_tile((utils.offsets4[dir_index_next]))
            dir_index_prev=dir_index-1
            if dir_index_prev<0:
                dir_index_prev=len(utils.offsets4)-1
            add_dir_tile((utils.offsets4[dir_index_prev]))
            
    def _draw_viewcone(self,lvl,color):
        """Show the viewcone on the level."""
        for x,y in self.viewcone.keys():
            lvl.tint.bg[x][y]=color
            lvl.tint.fade[x][y]=0.4
            
    def _see_player(self,lvl):
        """Now actually checking all the viewcone for player objects. Slower, yes, but should cover more future cases, e.g. distraction dummies."""
        seen_players=[]
        dists=[]
        for loc in self.viewcone:
            for obj in lvl.objects[loc]:
                if isinstance(obj, player.Player) and not obj.invisible:
                    seen_players.append(obj)
                    dists.append(abs(obj.x-self.x)+abs(obj.y-self.y))
        if seen_players:
            self.nearest_player=seen_players[dists.index(min(dists))]
            return True
        return False
    
class Guard(Hostile):
    def __init__(self, x, y, color=None):
        super(Guard, self).__init__(x, y)
        self.char = 'G'
        self.solid = True
        self.hitting = True
        self.color=(pygame.Color(120,30,40) if color is None else color)
        self.type=Guard_types.patroling
        self.route=[] #patrol route
        self.next_route_stop=0
        self.looking_around_ticks=0
        self.look_direction=(0,-1)
        self.path=[]
        self.last_player_pos=False
        self.angry=False
    def __draw_viewcone(self,lvl):
        self._draw_viewcone(lvl, pygame.Color(200,0,0) if self.angry else pygame.Color(208,100,0))
        
    def __looking_around(self,lvl):
        if self.looking_around_ticks:
            self.looking_around_ticks-=1
            dir_index=utils.offsets4.index(self.look_direction)
            dir_index=(dir_index+1)%len(utils.offsets4)
            self.look_direction=utils.offsets4[dir_index]
            self._regen_viewcone(lvl)
            return
    def __gen_route(self,lvl):
        number_of_points=random.choice((3,3,4)) #TODO: more random here?
        walkable=list(lvl.walkable.keys()) #TODO: better implementation?
        if lvl.custom_guard_pathing: rooms = tuple(lvl.rooms.keys())
        for x in range(1,number_of_points):
            for i in range(50): #try to find a reachable point
                point=random.choice(walkable) if not lvl.custom_guard_pathing else random.choice(rooms)
                if utils.bfs(self.x, self.y, lambda x,y: (x,y) in walkable, point[0], point[1]):
                    if not lvl.custom_guard_pathing or not self.route or i>=40 or len(utils.bfs(self.route[-1][0], self.route[-1][1], lambda x,y: (x,y) in walkable, point[0], point[1]))>=lvl.min_patrol_dist:
                        self.route.append(point)
                        break
        if not self.route: self.type=Guard_types.lazy
    def __pathfind(self,lvl,pos):
        self.path=utils.bfs(self.x, self.y,lambda x,y: lvl.is_walkable(x,y), pos[0], pos[1])
    def __step(self,lvl):
        #path could have changed!!
        if self.path: old_pos=self.path[-1]
        else: return False #check in case there is no path
        self.__pathfind(lvl,old_pos)
        #now we step
        if len(self.path)<=1:
            return False #failed to find path!
        x,y=self.path[1]
        #we throw away the old path anyways... so don't pop the element off
        self.look_direction=(x-self.x,y-self.y)
        self.x=x
        self.y=y
        self._regen_viewcone(lvl)
        self.__draw_viewcone(lvl)
        return True
    def hit(self, lvl, obj):
        if not (isinstance(obj, player.Player) or isinstance(obj, Guard)):
            self.remove=True
            lvl.get_floor(self.x,self.y).color=pygame.Color(255,0,0)

    def tick(self, lvl, actions):
        # few ideas:
        #   generate random patrol route
        #   lazy bastards (stand and turn?)
        mypos=(self.x,self.y)
        if self._see_player(lvl):
            self.__pathfind(lvl,(self.nearest_player.x,self.nearest_player.y))
            self.last_player_pos=(self.nearest_player.x,self.nearest_player.y)
            self.angry=True

        #now we have a path so walk it.
        if len(self.path)>0:
            if self.__step(lvl):#if we can't step, we resort to other logic
                return

        if self.last_player_pos and self.last_player_pos==mypos:
            self.looking_around_ticks=4
            self.last_player_pos=False
            self.angry=False

        self.__draw_viewcone(lvl)
        #now rest of logic
        if self.type==Guard_types.lazy:
            self.looking_around_ticks=4
            self.__looking_around(lvl)
            return
        elif self.type==Guard_types.patroling:
            if len(self.route)==0: #gen route if we don't have one
                self.__gen_route(lvl)

            if self.looking_around_ticks>0: # we need to look around. Either on the end of patrol, or player position
                self.__looking_around(lvl)
                return
            elif self.route[self.next_route_stop]==mypos:
                self.looking_around_ticks=4
                self.next_route_stop=(self.next_route_stop+1)%len(self.route)
                self.__pathfind(lvl,self.route[self.next_route_stop])
                self.angry=False
            else:
                self.next_route_stop=random.randint(0,len(self.route)-1)
                self.__pathfind(lvl,self.route[self.next_route_stop])
                self.angry=False
        else:
            raise Exception("Wrong guard type, sorry!")

class Lazy_Guard(Guard):
    def __init__(self, x, y, color=None):
        super().__init__(x, y, color)
        self.type=Guard_types.lazy
            
class Crossbow_Guard(Guard):
    """A stationary guard armed with a crossbow. It can move sideways!"""
    def __init__(self, x, y, color=None):
        """Make a guard armed with an xbow."""
        super().__init__(x, y, color)
        self.solid = False
        self.hitting = False
        self.char = 'b'
        self.type=Guard_types.crossbow
        self.station = (x, y)
        self.pos_expire=0
        self.look_direction=random.choice(utils.offsets4) #spawn with random direction
        self.reload_time = 5
        self.reloading = 3
        self.shot = False
        self.pos_expire=-2
        self.drop = False
        self.queued = None

    def _regen_viewcone(self, lvl):
        self.viewcone=utils.viewcone(self.x,self.y,lambda x,y: not lvl.is_opaque(x,y),7,self.look_direction)

    def _is_walkable(self, x, y, lvl):
        return lvl.is_walkable(x,y)

    def _sign(self, x):
        if x==0: return 0
        if x>0: return 1
        return -1

    def __shoot(self, direction, lvl, actions):
        self.reloading = self.reload_time
        self.shot = True
        self.char = 'b'
        bolt=traps.Crossbow_Bolt(self.x, self.y, direction, 3)
        d=utils.offsets4[direction]
        if lvl.is_walkable(self.x+d[0]*3, self.y+d[1]*3): lvl.objects[(self.x+d[0]*3, self.y+d[1]*3)].append(bolt)
        bolt.tick(lvl, actions)
        bolt.ticked=True

    def hit(self, lvl, obj):
        super().hit(lvl, obj)
        if self.remove and not self.drop:
            inventory.gen_item(self.x, self.y, 'Crossbow bolt', lvl)
            self.drop = True

    def tick(self, lvl, actions):
        """Look for player and try to shoot him if seen."""
        direction=utils.offsets4.index(self.look_direction)
        #Determine player position
        if self._see_player(lvl):
            self.last_player_pos=(self.nearest_player.x,self.nearest_player.y)
            self.shot = False
            self.pos_expire=4
            self.queued = None
        else: self.pos_expire-=1
        
        if self.reloading:
            #Reload if not loaded
            self.reloading-=1
            if not self.reloading: self.char = 'B'
        elif self.pos_expire>0:
            dx=self.last_player_pos[0]-self.x
            dy=self.last_player_pos[1]-self.y
            close=((self._sign(dx),0) if abs(dx)>=abs(dy) else (0,self._sign(dy)))
            #If player location known, try to shoot him
            if (self.x==self.last_player_pos[0]) and (self.y==self.last_player_pos[1]) and not self.shot:
                #If player is in the same position as you, dive in a random direction and shoot at it
                valid_dirs=[]
                for dirs in range(4):
                    if self._is_walkable(self.x+utils.offsets4[dirs][0], self.y+utils.offsets4[dirs][1], lvl): valid_dirs.append(dirs)
                if len(valid_dirs)>0:
                    dive_dir = random.choice(valid_dirs)
                    dive = utils.offsets4[dive_dir]
                    self.x, self.y = self.x+dive[0],self.y+dive[1]
                    self.__shoot((direction+2)%4, lvl, actions)
            elif (self.x==self.last_player_pos[0] or self.y==self.last_player_pos[1]) and not self.shot:
                self.__shoot(utils.offsets4.index(close), lvl, actions)
            else:
                #Try to get into shooting position if cannot shoot straight
                #If that is an empty square, will shoot the same turn (to prevent stepping into a trap and shooting before dying to it. :)
                if min(abs(dy),abs(dx))<=3:
                    strafe=((self._sign(dx),0) if abs(dx)<abs(dy) else (0,self._sign(dy)))
                    if self._is_walkable(self.x+strafe[0]+close[0],self.y+strafe[1]+close[1], lvl):
                        if self._is_walkable(self.x+strafe[0],self.y+strafe[1], lvl):
                            self.x, self.y = self.x+strafe[0],self.y+strafe[1]
                            if (self.x==self.last_player_pos[0] or self.y==self.last_player_pos[1]) and not self.shot and len(lvl.objects[(self.x, self.y)])<2:
                                self.__shoot(utils.offsets4.index(close), lvl, actions)
                        else:
                            self.x, self.y = self.x+close[0],self.y+close[1]
                            if (self.x==self.last_player_pos[0] or self.y==self.last_player_pos[1]) and not self.shot and len(lvl.objects[(self.x, self.y)])<2:
                                self.__shoot(utils.offsets4.index(strafe), lvl, actions)
                    else:
                        self.x, self.y = self.x+close[0],self.y+close[1]
                    self._regen_viewcone(lvl)
                
        elif self.station != (self.x, self.y):
            #If lost player, try to return to station
            self.path=utils.bfs(self.x, self.y,lambda x,y: lvl.is_walkable(x,y), self.station[0], self.station[1])
            self._Guard__step(lvl)
            if self.pos_expire>-2: self.look_direction=utils.offsets4[direction]
            self._regen_viewcone(lvl)
        elif self.queued is not None:
            self.look_direction = utils.offsets4[self.queued]
            self._regen_viewcone(lvl)
            self.queued = None
        elif random.randint(0,2)!=0:
            #look around
            valid_dirs=[]
            for dirs in range(4):
                if self._is_walkable(self.x+utils.offsets4[dirs][0], self.y+utils.offsets4[dirs][1], lvl): valid_dirs.append(dirs)
            if len(valid_dirs)>1 and (direction in valid_dirs): valid_dirs.remove(direction)
            if len(valid_dirs)>1 and ((direction+2)%4 in valid_dirs): valid_dirs.remove((direction+2)%4)
            if len(valid_dirs)>0:
                new_dir=random.choice(valid_dirs)
                if new_dir%2==direction%2:
                    self.queued = new_dir
                    self.look_direction = utils.offsets4[(new_dir+random.choice((-1,1)))%4]
                else: self.look_direction = utils.offsets4[new_dir]
            self._regen_viewcone(lvl)
        #draw the viewcone
        self._Guard__draw_viewcone(lvl)
            

def generate(lvl,walkable,numberGuards=3, numberCrossbowGuards=1, min_dist_to_guard=5):
    #place the guards using new experimental method
    utils.place(lvl, walkable, Crossbow_Guard, numberCrossbowGuards, 8, min_dist_to_guard+1, True)
    utils.place(lvl, walkable, Guard, numberGuards, 3, min_dist_to_guard+1, False)
