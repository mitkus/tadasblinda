import pygame
from lib import utils

class Effects:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.clear()

    def clear(self):
        self.bg = utils.list2d(self.width, self.height, pygame.Color(0, 0, 0))
        self.fade=utils.list2d(self.width, self.height, 0)

    def render(self, console):
        for y in range(self.height):
            for x in range(self.width):
                console.set_color(x, y+1, 1, None, self.bg[x][y])
    def update(self):
        for y in range(self.height):
            for x in range(self.width):
                c=self.bg[x][y]
                f=self.fade[x][y]
                self.bg[x][y]=pygame.Color(int(c.r*f),int(c.g*f),int(c.b*f))
                
    def smoke(self, x, y):
        self.bg[x][y]=pygame.Color(68,68,78)
        self.fade[x][y]=0.2

