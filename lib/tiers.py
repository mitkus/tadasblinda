""" For managing level progression """
from lib import utils
import math, random

def setup(tier):
	# default object amounts in a level
	counts = {'number_guards':3,
	          'number_boulders':2,
	          'number_pots':3,
	          'number_spiders':6,
	          'number_kittens':1,
	          'number_smoke_bug':3,
	          'number_power_source':5,
	          'number_snake':1,
	          'number_items':4,
	          'number_crossbow_guards':1,
	          'exit_difficulty':15,
	          'give_sapling':1
	          }
	if tier:
		for key in counts.keys():
			counts[key] = 0
		if tier == 2:
			counts['number_guards'] = 1
		elif tier == 3:
			counts['number_power_source'] = 1
			counts['exit_difficulty'] = 1
		elif tier == 4:
			counts['number_guards'] = 1
			counts['number_power_source'] = 3
			counts['exit_difficulty'] = 6
		elif tier == 5:
			counts['number_guards'] = 1
			counts['number_power_source'] = 3
			counts['exit_difficulty'] = 6		
			counts['number_items'] = 4
		elif tier == 6:
			counts['number_guards'] = 4
			counts['number_power_source'] = 2
			counts['exit_difficulty'] = 4
			counts['number_pots'] = 5
		elif tier == 7:
			counts['number_guards'] = 1
			counts['number_power_source'] = 4
			counts['exit_difficulty'] = 6		
			counts['number_spiders'] = 6
			counts['give_sapling'] = 1
		elif tier == 7:
			counts['number_guards'] = 1
			counts['number_power_source'] = 4
			counts['exit_difficulty'] = 6		
			counts['number_spiders'] = 6
			counts['give_sapling'] = 1
		elif tier == 8:
			counts['number_guards'] = 4
			counts['number_power_source'] = 4
			counts['exit_difficulty'] = 6		
			counts['number_spiders'] = 2
			counts['give_sapling'] = 1
			counts['number_boulders'] = 5
		elif tier == 9:
			counts['number_guards'] = 2
			counts['number_snake'] = 6
			counts['exit_difficulty'] = 7		
			counts['number_spiders'] = 2
			counts['give_sapling'] = 1
			counts['number_items'] = 5
			counts['number_smoke_bug'] = 4
		elif tier == 10:
			counts['number_crossbow_guards'] = 4
			counts['number_power_source'] = 6
			counts['exit_difficulty'] = 10		
			counts['number_spiders'] = 2
			counts['give_sapling'] = 1
			counts['number_items'] = 6
			counts['number_smoke_bug'] = 2
		elif tier > 10:
			counts['number_guards'] = tier // 2 - tier // 4
			counts['number_boulders'] = tier // 4
			counts['number_pots'] = (tier // 3) % 3
			counts['number_spiders'] = 2 + tier // 4
			counts['number_kittens'] = tier // 5
			if tier // 5 > 2:counts['number_kittens'] = 2
			counts['number_smoke_bug'] = 0
			if tier > 1:
				counts['number_smoke_bug'] = 1 + tier // 5
			counts['number_power_source'] = tier // 4
			if 10 - tier // 2 < 5: counts['number_power_source'] = 5
			counts['number_snake'] = 0
			if tier > 6: counts['number_snake'] = 1
			counts['number_items'] = tier // 2
			counts['number_crossbow_guards'] = tier // 5
			counts['exit_difficulty'] = counts['number_power_source'] * (tier // 10 + 1)
			counts['give_sapling'] = 1

	return counts

def setup_campaign(tier):
    return setup(tier+11)
    
def setup_tutorial(tier):
    return setup(tier+1)
    
def setup_map(tier, size, maze=False):
    counts = {}
    counts['number_smoke_bug']=int(size/(60-math.log(tier+1,2)*2)/(random.random()+0.25))
    counts['number_boulders']=int(size//(200*(random.random()+0.7)))
    counts['number_guards']=int(size**0.8//(100/(random.random()*math.log(tier+1,3)+3)))+maze
    counts['number_crossbow_guards']=max((random.randint(round(math.log(tier+1,10)),round(math.log(tier+1,5)))-1*maze),0)
    counts['number_kittens']=random.choice((1,)*10+(2,)+(0,)*3)
    counts['number_items']=2 if tier==0 else random.choice((1,1,2))
    counts['number_pots']=int(max(0,size//(80+tier*5+random.random()*70)))
    counts['number_spiders']=int(size/80/(random.random()+0.50))
    #chance of it being a tree level
    if random.random() > 1/(tier/10+1):
        counts['give_sapling']=1
        counts['exit_difficulty']=5*random.randint(int(math.sqrt(tier//7+1)),int((tier//5+1)**0.65))
        counts['number_power_source']=random.randint(round(counts['exit_difficulty']*0.22+1),round(counts['exit_difficulty']*0.29+2))
        counts['number_snake']=random.randint(round(math.log(tier+1,10)),round(math.log(tier+1,10)+1))
        counts['number_spiders']=max(2,counts['number_spiders'])
    else:
        counts['number_power_source']=0
        counts['number_snake']=0
        counts['exit_difficulty']=None
        counts['exit_num']=random.choice((1,)*10+(2,))
    return counts
    
def setup_dragon(tier, size):
    counts = {}
    counts['number_smoke_bug']=int(size/(170-math.log(tier+1,2)*2)/(random.random()+0.25))
    counts['number_boulders']=int(size//(300*(random.random()+0.7)))
    counts['number_guards']=int(size**0.8//(270/(random.random()*math.log(tier+1,4)+3)))+1
    counts['number_crossbow_guards']=0
    counts['number_kittens']=random.choice((1,)*10+(2,)+(0,)*3)
    counts['number_items']=random.randint(0,1)
    counts['number_pots']=int(max(0,size//(240+tier+random.random()*30)))
    counts['number_spiders']=int(size/120/(random.random()+0.50))
    counts['number_power_source']=0
    counts['number_snake']=0
    counts['exit_difficulty']=None
    counts['exit_num']=2
    return counts
    
def setup_classic(tier):
    return {'number_guards':3,
            'number_boulders':2,
            'number_pots':3,
            'number_spiders':6,
            'number_kittens':1,
            'number_smoke_bug':4,
            'number_power_source':5,
            'number_snake':1,
            'number_items':2,
            'number_crossbow_guards':1,
            'exit_difficulty':15,
            'give_sapling':1
            }
            
def setup_minimalist(tier):
    return {'number_guards':0,
            'number_boulders':0,
            'number_pots':0,
            'number_spiders':0,
            'number_kittens':0,
            'number_smoke_bug':0,
            'number_power_source':0,
            'number_snake':0,
            'number_items':0,
            'number_crossbow_guards':0,
            'exit_difficulty':None,
            'give_sapling':0
            }
            
level_setup = dict(zip(utils.gamemodes,(setup_tutorial, setup_map, setup_classic, setup_minimalist)))
