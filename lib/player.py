from lib import inventory, traps, guard, critters, creatures, entity
from lib import utils, effects2
import pygame
import random

class Player(entity.Entity):
    def __init__(self, x, y):
        super(Player, self).__init__(x, y)
        self.char = '@'
        self.hitting = True
        self.inventory = inventory.Inventory()
        self.invisible = 0
        self.power_charge = 0
        self.victory = False
        self.hidden = False
        self.solid = False
        self.haste = 0
        self.haste_turn = False
        self.ninja_smoke = False
        self.marker = False

    def tick(self, lvl, actions):
        action_done = False
        self.haste_turn = False
        self.ninja_smoke = False
        for action in actions:
            if action[0] in list(range(4)):
                b = 1
                dx, dy = utils.offsets4[action[0]]
                nx, ny = self.x+dx*b, self.y+dy*b
                if lvl.is_walkable(nx, ny):
                    self.x, self.y = nx, ny
                    action_done = True
            elif action[0] in list(utils.item_action_letters):
                action_done = self.inventory.use_item(self, action[0], lvl)
            elif action[0] == 999:
                action_done = True
                #If hiding in a pot, get hiding status and become invisible. Moved from pot logic so status is always obtained before guard movement.
                for obj in lvl.objects[(self.x,self.y)]:
                    if isinstance(obj,traps.Pot) and obj.open>2:
                        self.hidden = True
                        self.hitting = False
                        self.invisible = max(self.invisible, 2)
            elif action[0] == 'l':
                action_done = effects2.animator.lightning(self.x, self.y, lvl) #call lightning
            #remove hiding status if pot left
            if action[0] != 999 and self.hidden:
                self.hidden = False
                self.hitting = True
        if self.haste and action_done and not self.haste_turn:
            lvl.tint.bg[self.x][self.y]=pygame.Color(100,0,100)
            lvl.tint.fade[self.x][self.y]=0.7
            if random.random()<0.6:
                self.haste-=1
                self.haste_turn=True
        if action_done:
            if self.invisible and not self.haste_turn:
                self.invisible -= 1
                if self.invisible: #everyone else, including guards, act AFTER the player, so they will see him this turn already.
                    lvl.tint.bg[self.x][self.y]=pygame.Color(100,100,100)
                    lvl.tint.fade[self.x][self.y]=0.9
            return True
        return False
    
    def hit(self, room, obj):
        #Hidden protects from everything except boulders, ninja smoke protects against everything except boulders and fire, other players don't kill you and the dragon won't crush you if you're invisible.
        if (isinstance(obj, traps.Boulder) or not (self.hidden or (self.ninja_smoke and not isinstance(obj, creatures.Fire)))) and not isinstance(obj, Player) and not (isinstance(obj, creatures.Dragon) and self.invisible):
            death_color = pygame.Color(255,55,55)
            if not self.victory:
                if isinstance(obj, traps.Boulder):
                    self.inventory.set_message("A huge boulder crushes you.", color=death_color)
                elif isinstance(obj, traps.PlaceableTrap):
                    self.inventory.set_message("You stepped into your own trap.", color=death_color)
                elif isinstance(obj, guard.Guard):
                    self.inventory.set_message("A guard quickly dispatched you.", color=death_color)
                elif isinstance(obj, traps.Crossbow_Bolt):
                    self.inventory.set_message("You were shot with a crossbow bolt.", color=death_color)
                elif isinstance(obj, effects2.Blink_Effect):
                    self.inventory.set_message("You teleported into something solid.", color=death_color)
                elif isinstance(obj, creatures.Fire):
                    self.inventory.set_message("You burned to a crisp.", color=death_color)
                elif isinstance(obj, creatures.Dragon):
                    self.inventory.set_message("The dragon crushed you utterly.", color=death_color)
                if not self.remove and room.is_walkable(self.x,self.y):
                    for item in self.inventory.contents:
                        room.objects[(self.x, self.y)].append(item)
                        item.x, item.y = self.x, self.y
                        item.remove = False
                    if self.power_charge>=1:
                        room.objects[(self.x, self.y)].append(critters.Power_Source(self.x,self.y))
            self.remove = True
    

class RoomExit(entity.Entity):
    def __init__(self, x, y):
        super(RoomExit, self).__init__(x, y)
        self.color = pygame.Color(0, 255, 0)
        self.char = '>'
        self.solid = False

    def hit(self,lvl,obj):
        if isinstance(obj, Player):
            lvl.victory=True
            obj.victory=True
